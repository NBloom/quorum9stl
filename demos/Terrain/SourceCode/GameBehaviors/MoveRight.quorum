use Libraries.Containers.Array
use Libraries.Containers.Iterator
use Libraries.Interface.Behaviors.Behavior
use Libraries.Interface.Events.BehaviorEvent
use Libraries.Interface.Item
use Libraries.Interface.Item3D
use Libraries.Interface.Events.KeyboardEvent
use Libraries.Interface.Events.MouseEvent
use Libraries.Game.Game
use Libraries.Game.GameStateManager
use Libraries.Game.InputMonitor
use Libraries.Game.Graphics.Model
use Libraries.Game.Graphics.ModelBlueprint

use Libraries.Game.Terrain
use Libraries.Game.Terrain.TriangleModel

class MoveRight is Behavior
    GameStateManager gameStateManager //Get the gamestatemanager in order to retrieve the current game
    Game currentGame
    InputMonitor inputMonitor
    KeyboardEvent keyboardEvent //keyboard event that i use incase i need to refer to certain keys or test that a keyboard event occured
    SelectionManager selectionManager

    action Run(BehaviorEvent event)
        /*get current game*/
        currentGame = gameStateManager:GetGame() //retrieve the current game

        /*If selection manager is undefined or has an empty array then nothing has been selected.
           Else, iterate through the array of items and output was has been copied*/
        if inputMonitor:IsKeyPressed(keyboardEvent:ALT_LEFT) = true and inputMonitor:IsKeyPressed(keyboardEvent:RIGHT) = true and inputMonitor:NumberOfKeysPressed() = 2
            if  selectionManager = undefined or selectionManager:GetSelectionCount() = 0
                output "Nothing has been selected."
            elseif selectionManager:GetSelectionCount() > 0
            Iterator <Item3D> itemIterator = GetIterator(selectionManager)
                repeat while itemIterator:HasNext()
                    Item3D item3D = itemIterator:Next()
                    if item3D  not= undefined  and item3D:GetName() not= undefined and item3D:GetName() not= "Unnamed"
                        /*if the current3D is a Terrain model, grab the Terrain's mesh and move all triangle models.
                          else, move the item3D*/
                        if FindTerrain(item3D) not= undefined
                            Array<TriangleModel> mesh = FindTerrain(item3D):GetMesh()
                            integer meshIndex = 0
                            repeat mesh:GetSize() times
                                Model currentTriangleModel = mesh:Get(meshIndex)
                                currentTriangleModel:SetX(currentTriangleModel:GetX()+0.5)
                                meshIndex = meshIndex + 1
                            end //repeat end
                        else
                            item3D:SetX(item3D:GetX()+0.5)
                        end //end FindTerrain end
                    end //end if that is within repeat
                end //end repeat
            end //end elseif
        end //end if
    end //end action

    /*Used to empty a the array of a given SelectionManager*/
    action EmptySelection(SelectionManager emptyManager)
        emptyManager:EmptyArray()
    end

    action FindTerrain(Item3D item3D) returns Terrain
        Iterator <Terrain> terrainIterator = selectionManager:GetTerrainArray():GetIterator()
        repeat while terrainIterator:HasNext()
            Terrain nextTerrain = terrainIterator:Next()
                if nextTerrain not= undefined
                    if item3D:Equals(nextTerrain)
                        return nextTerrain
                    end
                end
         end
        return undefined
    end

    /*Return the name of this behavior*/
    action GetBehaviorName() returns text
        return "Move Right"
    end

    /*Retrieve the iterator of the given SelectionManager's array*/
    action GetIterator(SelectionManager copyManager) returns Iterator<Item3D>
        return copyManager:GetSelectionArray():GetIterator()
    end

    /*Get the Behavior's SelectionManager*/
    action GetSelectionManager() returns SelectionManager
        return selectionManager
    end

    /*Set the main SelectionManager*/
    action SetSelectionManager(SelectionManager copyManager)
        selectionManager = copyManager
    end
end