use Libraries.Containers.Array
use Libraries.Containers.Iterator
use Libraries.Interface.Behaviors.Behavior
use Libraries.Interface.Events.BehaviorEvent
use Libraries.Game.Game
use Libraries.Game.GameStateManager
use Libraries.Interface.Item
use Libraries.Interface.Item3D
use Libraries.Game.Terrain

class SelectionManager
   /*Array of Item3D objects that will be used to store all items that have been selected*/
    private Array<Item3D> selectionArray
    private Array<Terrain> terrainArray

    /*objects used to determine the count of items selected and the index of the last item selected*/
    integer selectionCount = 0
    integer currentSelectionIndex = 0

    /*will get the game's seconds that can be used for computation purposes*/
    number seconds = 0

    /*Adds an Item3D to the selectionArray and adjusts the index/count*/
    action AddSelection(Item3D selectionItem)
        //selectionArray:Add(selectionCount, selectionItem)
        selectionArray:Add(selectionItem)
        selectionCount = selectionCount + 1
        currentSelectionIndex = selectionCount - 1
    end

    /*Checks if the selectionArray contains the item given in the parameter and returns a boolean*/
    action Contains(Item3D item3D) returns boolean
        return selectionArray:Has(item3D)
    end

    /*Copies the array given as a parameter into the class's selectionArray*/
    action CopyArray(Array<Item3D> newSelectionArray)
        Object selectionObject = newSelectionArray:Copy()
        selectionArray = cast(Array<Item3D>, selectionObject)
        SetSelectionCount(newSelectionArray:GetSize())
        SetCurrentIndex(newSelectionArray:GetSize() - 1)
    end

    /*Copies the data from selectionManager given as a parameter*/
    action CopySelectionManager(SelectionManager copySelectionManager) 
        Array<Item3D> newArray = copySelectionManager:GetSelectionArray()
        CopyArray(newArray)
        seconds = copySelectionManager:GetSeconds()
    end

    /*Empties the selection array in this class*/
    action EmptyArray()
        selectionArray:Empty()
        currentSelectionIndex = 0
        selectionCount = 0
    end

    /*get index of the last item added*/
    action GetCurrentSelectionIndex() returns integer
        return currentSelectionIndex
    end

    /*Get's SelectionArray's Iterator*/
    action GetIterator() returns Iterator<Item3D>
        return selectionArray:GetIterator()
    end

    /*Get an Item3D from the selectionArray using an integer location*/
    action GetSelection(integer selectionIndex) returns Item3D
        if selectionIndex < 0 or selectionIndex >= GetSelectionCount()
            output "Integer provided invalid"
        elseif selectionArray:Get(selectionIndex) = undefined
            output "An item does not exist in this location"
        else 
            return selectionArray:Get(selectionIndex)
        end
        return undefined
    end

    /*Get an Item3D from the selectionArray using an Item3D*/
    action GetSelection(Item3D item3D) returns Item3D
        if selectionArray:Has(item3D)
            return GetSelection(selectionArray:GetFirstLocation(item3D))
        else 
            output item3D:GetName() + " does not exist in the array" 
        end
        return undefined
    end

    /*Return's the selection manager's array*/
    action GetSelectionArray() returns Array<Item3D>
        return selectionArray
    end

    /*Get the count of items in the selectionArray*/
    action GetSelectionCount() returns integer
        return selectionCount
    end

    action GetTerrainArray() returns Array<Terrain>
        return terrainArray
    end

    /*Added this action so that each action that requires the game can retrieve when it is called
    and not potentially grab the game before the game is actually created, this would cause it to be undefined
    and crash*/
    private action GetGame() returns Game
        GameStateManager gameStateManager //get game state manager to add behaviors and GameCameraClass
        if gameStateManager:GetGame() not= undefined
            return gameStateManager:GetGame()
        else 
            output "Game is undefined. The game may have not been created yet."
        end
        return undefined
    end

    /*Return the seconds integer*/
    action GetSeconds() returns number
        return seconds
    end

    /*Remove the item that was passed if it exists in the array*/
    action RemoveItem(Item3D item3D)
        if selectionArray:Has(item3D) and selectionArray:GetSize() > 0 and selectionCount > 0
            selectionArray:Remove(item3D)
            selectionArray:Sort()
            selectionCount = selectionArray:GetSize()
        else
            output "Item does not exist in array or no more items to remove"
        end
    end

    /*Set the count of items in the array*/
    action SetSelectionCount(integer count)
        selectionCount = count
    end
    
    /*Set the index of the last item added*/
    action SetCurrentIndex(integer index)
        currentSelectionIndex = index
    end

    /*Set the game seconds*/
    action SetSeconds(number gameSeconds)
        seconds = gameSeconds
    end
    
    /*set terrain array to the game's terrain array*/
    action SetTerrainArray(Array<Terrain> setTerrainArray)
        terrainArray = setTerrainArray
    end
end