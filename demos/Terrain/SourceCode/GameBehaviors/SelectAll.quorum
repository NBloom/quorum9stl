use Libraries.Containers.Array
use Libraries.Containers.Iterator
use Libraries.Interface.Behaviors.Behavior
use Libraries.Interface.Events.BehaviorEvent
use Libraries.Interface.Item
use Libraries.Interface.Item3D
use Libraries.Interface.Events.KeyboardEvent
use Libraries.Interface.Events.KeyboardListener
use Libraries.Interface.Events.MouseEvent
use Libraries.Interface.Events.MouseListener
use Libraries.Game.Game
use Libraries.Game.GameStateManager
use Libraries.Game.Layer3D
use Libraries.Game.Graphics.Model
use Libraries.Game.Graphics.ModelBlueprint

class SelectAll is Behavior
    GameStateManager gameStateManager //Get the gamestatemanager in order to retrieve the current game
    Game currentGame
    SelectionManager selectionManager

    action Run(BehaviorEvent event)
        currentGame = gameStateManager:GetGame() //retrieve the current game
        Layer3D currentLayer3D = currentGame:GetCurrentLayer3D() //get currentGame's current Layer3D
        Iterator <Item3D> itemIterator = currentLayer3D:GetIterator() //get current Layer3D's item iterator

        /*empty array of what has been selected to avoid duplicates*/
        EmptySelection(selectionManager)

        /*iterate through the currentGame's current Layer3D and grab all items and add them to the SelectionManager if they are defined*/
        repeat while currentLayer3D:GetSize() > 0 and itemIterator:HasNext()
            Item3D layerItem = itemIterator:Next()
                if layerItem not= undefined
                    selectionManager:AddSelection(layerItem)
                    //output layerItem:GetName() + " item(s) selected"
                end
        end

        output selectionManager:GetSelectionArray():GetSize() + " item(s) currently selected"
    end

    /*Used to empty a the array of a given SelectionManager*/
    action EmptySelection(SelectionManager emptyManager)
        emptyManager:EmptyArray()
    end

    /*Return the name of this behavior*/
    action GetBehaviorName() returns text
        return "Select All"
    end

    /*Get the Behavior's SelectionManager*/
    action GetSelectionManager() returns SelectionManager
        return selectionManager
    end

    /*Set the main SelectionManager*/
    action SetSelectionManager(SelectionManager copyManager)
        selectionManager = copyManager
    end
end
