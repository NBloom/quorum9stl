//how to use
//wasd for forward, left, back, right
//leftshift + w, s for up, down

//up, down for pitch camera rotation
//left, right for yaw camera rotation
//leftshift + left, right for roll camera rotation
//leftcontrol + left, right for y-axis camera rotation

//z to switch camera views

use Libraries.Game.Graphics.Camera
use Libraries.Compute.Vector3 
use Libraries.Interface.Events.KeyboardListener
use Libraries.Interface.Events.KeyboardEvent
use Libraries.Game.InputMonitor
use Libraries.Compute.Frustum
use Libraries.Game.Graphics.Renderable
use Libraries.Game.Graphics.Model
use Libraries.Containers.Array
use Libraries.Compute.Matrix4
use Libraries.Game.Graphics.PerspectiveCamera

use Libraries.Game.Terrain.Octree

class CameraListener

    //if state = 1, using main camera
    //if state = 2, using birdsEye camera
    private integer state = 0
    private boolean zKeyReleased = true

    //need to save camera pos/orientation data
    //we can add more cameras dynamically if we need using an array
    //so right now this isn't scalable
    CameraData mainCamData
    CameraData birdsEyeData

    constant number speedX = 0.1
    constant number speedY = 0.1
    constant number speedZ = 0.1
    constant number rotSpeedX = 50
    constant number rotSpeedY = 50

    action checkState() returns integer
        return state
    end

    action setState(integer newState)
        if newState > 0 and newState < 3
            state = newState
        end
    end
    
    action initializeCam(Camera camera, integer camNum)
        setState(1)
        if camNum = 1
            mainCamData:SetCamera(camera)
        elseif camNum = 2
            birdsEyeData:SetCamera(camera)
        end
    end

    action checkKeys(Camera camera, PerspectiveCamera pCamera, number seconds)
        if checkState() = 1
            mainCamData:SetCamera(camera)
            controlMainCamera(camera,seconds)
        elseif checkState() = 2
            birdsEyeData:SetCamera(camera)
            controlMainFrustum(camera, pCamera, seconds)
        end
    end

    action controlMainCamera(Camera camera, number seconds)
        InputMonitor monitor
        KeyboardEvent keys
        Vector3 axis
        Vector3 currentDirection
        Vector3 up
        Vector3 planeVector
        Vector3 crossProduct

        if monitor:IsKeyPressed(keys:Z)
            //check if Z key is released
            //need a flag, otherwise it will keep switching
            if zKeyReleased = true
                zKeyReleased = false

                //update state and save current camera data
                setState(2)
                mainCamData:SetCamera(camera)

                //update camera with other camera data
                camera:SetZoom(birdsEyeData:GetZoom())
                camera:SetPosition(birdsEyeData:GetPosition())
                camera:SetDirection(birdsEyeData:GetDirection())
                camera:SetUp(birdsEyeData:GetUp())
            end
        elseif zKeyReleased = false
            zKeyReleased = true
        end

        if monitor:IsKeyPressed(keys:W)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()
           
            if monitor:IsKeyPressed(keys:SHIFT_LEFT)
                axis:Set(up)
                axis:Scale(speedY)
            else
                axis:Set(currentDirection)
                axis:Scale(speedZ)
            end
                camera:Move(axis)
        end
        if monitor:IsKeyPressed(keys:S)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()
           
            if monitor:IsKeyPressed(keys:SHIFT_LEFT)
                axis:Set(up)
                axis:Scale(speedY * -1)
            else
                axis:Set(currentDirection)
                axis:Scale(speedZ * -1)
            end
                camera:Move(axis)
        end
        if monitor:IsKeyPressed(keys:A)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()
            planeVector:Set(up)
            axis:Set(planeVector:CrossProduct(currentDirection))
            axis:Scale(speedX * -1)
            camera:Move(axis)
        end
        if monitor:IsKeyPressed(keys:D)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()
            planeVector:Set(up)
            axis:Set(planeVector:CrossProduct(currentDirection))
            axis:Scale(speedX * 1)
            camera:Move(axis)
        end
        if monitor:IsKeyPressed(keys:UP)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()
            planeVector:Set(up)
            axis:Set(planeVector:CrossProduct(currentDirection))
            camera:Rotate(axis, -1 * rotSpeedX * seconds)
        end
        if monitor:IsKeyPressed(keys:DOWN)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()
            planeVector:Set(up)
            axis:Set(planeVector:CrossProduct(currentDirection))
            camera:Rotate(axis, rotSpeedX * seconds)
        end
        if monitor:IsKeyPressed(keys:LEFT)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()

            if monitor:IsKeyPressed(keys:SHIFT_LEFT)
                axis:Set(currentDirection)
                camera:Rotate(axis, rotSpeedY * seconds)
            
            elseif monitor:IsKeyPressed(keys:CONTROL_LEFT)
                axis:Set(up)
                camera:Rotate(axis, -1 * rotSpeedY * seconds)
            else
                axis:Set(0, 1, 0) 
                camera:Rotate(axis, -1 * rotSpeedY * seconds)
            end
        end
        if monitor:IsKeyPressed(keys:RIGHT)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()

            if monitor:IsKeyPressed(keys:SHIFT_LEFT)
                axis:Set(currentDirection)
                camera:Rotate(axis, -1 * rotSpeedY * seconds)

            elseif monitor:IsKeyPressed(keys:CONTROL_LEFT)
                axis:Set(up)
                camera:Rotate(axis, rotSpeedY * seconds) 
            else
                axis:Set(0, 1, 0) 
                camera:Rotate(axis, rotSpeedY * seconds)
            end
        end
    end

    action controlMainFrustum(Camera camera, PerspectiveCamera pCamera, number seconds)
        InputMonitor monitor
        KeyboardEvent keys
        Vector3 axis
        Vector3 currentDirection
        Vector3 up
        Vector3 planeVector
        Vector3 crossProduct

        if monitor:IsKeyPressed(keys:Z) 
            //check if Z key is released
            //need a flag, otherwise it will keep switching
            if zKeyReleased = true
                zKeyReleased = false

                //update state and save current camera data
                setState(1)
                birdsEyeData:SetCamera(camera)
    
                //update camera with other camera data
                camera:SetZoom(mainCamData:GetZoom())
                camera:SetPosition(mainCamData:GetPosition())            
                camera:SetDirection(mainCamData:GetDirection())
                camera:SetUp(mainCamData:GetUp())
            end
        elseif zKeyReleased = false
            zKeyReleased = true
        end

        //The below are all using the perspective camera in order to do their transformations
        pCamera:SetPosition(mainCamData:GetPosition())
        pCamera:SetDirection(mainCamData:GetDirection())
        pCamera:SetUp(mainCamData:GetUp())
 
        if monitor:IsKeyPressed(keys:W)
            axis:Set(0, 0, 1)
            axis:Scale(speedZ)
            pCamera:Move(axis)
        end
        if monitor:IsKeyPressed(keys:S)
            axis:Set(0, 0, 1)
            axis:Scale(speedZ * -1)
            pCamera:Move(axis)
        end
        if monitor:IsKeyPressed(keys:A)
            axis:Set(1, 0, 0)
            axis:Scale(speedX * -1)
            pCamera:Move(axis)
        end

        if monitor:IsKeyPressed(keys:D)
            axis:Set(1, 0, 0)
            axis:Scale(speedX * 1)
            pCamera:Move(axis)
        end

/*        if monitor:IsKeyPressed(keys:UP)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()
            planeVector:Set(up)
            axis:Set(planeVector:CrossProduct(currentDirection))
            camera:Rotate(axis, -1 * rotSpeedX * seconds)
        end
        if monitor:IsKeyPressed(keys:DOWN)
            currentDirection = camera:GetDirection()
            up = camera:GetUp()
            planeVector:Set(up)
            axis:Set(planeVector:CrossProduct(currentDirection))
            camera:Rotate(axis, rotSpeedX * seconds)
        end
*/
        if monitor:IsKeyPressed(keys:LEFT)
            axis:Set(0, 1, 0) 
            pCamera:Rotate(axis, -1 * rotSpeedY * seconds)
        end
        if monitor:IsKeyPressed(keys:RIGHT)
            currentDirection = mainCamData:GetDirection()
            up = mainCamData:GetUp()
            axis:Set(0, 1, 0) 
            pCamera:Rotate(axis, rotSpeedY * seconds)
        end
        
        pCamera:Update()
        mainCamData:UpdateCameraData(pCamera:GetPosition(),pCamera:GetDirection(),pCamera:GetUp(),pCamera:GetInverseCombinedMatrix())
    end


     action checkRenderablesOct(Octree octree)     
        mainCamData:UpdateFrustum()
        octree:SetFrustum(mainCamData:GetFrustum())
     end
end