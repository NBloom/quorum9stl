package Libraries.Containers.Octree

use Libraries.Containers.List
use Libraries.Containers.Array
use Libraries.Compute.Vector3
use Libraries.Compute.BitwiseOperations
use Libraries.Containers.Table
use Libraries.Compute.Frustum
use Libraries.Compute.Math
use Libraries.Game.Graphics.Model

//This class is used to create the nodes for the octree class
class OctreeNode
    public Vector3 position //where cube is in space
    public number size = 0  //how large cube is
    private number halfsize = 0
    private number radius = 0
    public Array<OctreeNode> subNodes //8 subnodes
    List<Model> values //list of values in leaf node 

    //Parameterized constructor
    action CreateOctreeNode(Vector3 pos, number createSize)
        Math math
        position = pos
        size = createSize
        halfsize = size/2
        radius = math:SquareRoot(3*halfsize*halfsize)
    end

    //Divide nodes
    action Subdivide(integer depth)
        BitwiseOperations ops
        Array<OctreeNode> arr
        arr:SetSize(8)
        subNodes = arr

        integer i = 0
        repeat while i<subNodes:GetSize()
            Vector3 newPos=position:Copy()

             //divide right/left sections
            if ops:And(i,4) not= 0
               newPos:SetX(newPos:GetX()+(size*0.25))
            else
                newPos:SetX(newPos:GetX()-(size*0.25))
            end

            //divide top/bottom sections
            if ops:And(i,2) not= 0
               newPos:SetY(newPos:GetY()+(size*0.25))
            else
                newPos:SetY(newPos:GetY()-(size*0.25))
            end

            //divide front/back sections
            if ops:And(i,1) not= 0
               newPos:SetZ(newPos:GetZ()+(size*0.25))
            else
                newPos:SetZ(newPos:GetZ()-(size*0.25))
            end

            OctreeNode new
            new:CreateOctreeNode(newPos, size*0.5)
            subNodes:Set(i,new)
            if depth>1
              subNodes:Get(i):Subdivide(depth-1)  
            end
            i = i+1
        end
    end
    
    //Returns a value of a node in the tree
    action GetIndexOfPosition(Vector3 lpos) returns integer
        BitwiseOperations ops
        integer index=0
        integer result = index
        //if pos we are finding is right/left of our cube
        if lpos:GetX() > position:GetX()
            index = ops:Or(index,4)
        end
        //if pos we are finding is above/below our cube
        if lpos:GetY() > position:GetY()
            index = ops:Or(index,2)
        end
        //if pos we are finding is in front/back of our cube
        if lpos:GetZ() > position:GetZ()
           index = ops:Or(index,1)
        end
        return index  
    end 

    //Adds Triangle Models to the octree
    action AddTriangle(Model triangle)
        if IsLeaf()
            values:Add(triangle)
        else
            index1 = GetIndexOfPosition(triangle:GetTriPos1())
            index2 = GetIndexOfPosition(triangle:GetTriPos2())
            index3 = GetIndexOfPosition(triangle:GetTriPos3())
    
            if index1=index2 and index2=index3
                subNodes:Get(index1):AddTriangle(triangle)
            else
                values:Add(triangle)
            end
        end
    end

    //Deletes empty nodes
    action PruneTree returns integer
        integer removed = 0
        integer i = 0
        repeat subNodes:GetSize() times
            removed = removed + subNodes:Get(i):PruneTree()
            if subNodes:Get(i):IsEmpty()
                subNodes:RemoveAt(i)
                removed = removed + 1
            else
                i=i+1
            end
        end
        return removed
    end

    //Returns true if octree is empty and false if not
    action IsEmpty returns boolean
        if values:GetSize()=0 and subNodes:GetSize()=0
            return true
        else
            return false
        end
    end
    
    /*
    GetRenderables is used to retrieve all of the individual renderable
    components of this Model.
    */
    action GetRenderables(Libraries.Containers.Array<Libraries.Game.Graphics.Renderable> renderables, Frustum frustum)
        if frustum:SphereInFrustum(position,radius)
            index = 0
            repeat values:GetSize() times
                Model triangle = values:Get(index)
                Vector3 pos1 = triangle:GetTriPos1()
                Vector3 pos2 = triangle:GetTriPos2()
                Vector3 pos3 = triangle:GetTriPos3()         
                boolean inFrustum = frustum:PointInFrustum(pos1) or frustum:PointInFrustum(pos2) or frustum:PointInFrustum(pos3)
                if inFrustum
                     triangle:GetRenderables(renderables)
                end
                index = index+1
            end
            index=0
            repeat subNodes:GetSize() times
                subNodes:Get(index):GetRenderables(renderables, frustum)
                index = index+1           
            end
        end
    end
    
    //Returns true if node is a leaf
    action IsLeaf returns boolean
        return subNodes:IsEmpty()
    end
end
