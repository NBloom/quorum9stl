package Libraries.Compute.Statistics.Columns

use Libraries.Containers.Array
use Libraries.Compute.Statistics.DataFrameColumnCalculation
use Libraries.Compute.Statistics.DataFrameColumn

/*
    BooleanColumn is a DataFrameColumn that contains Number objects. These objects can be undefined
    or not. 

    Attribute: Author Andreas Stefik
*/
class BooleanColumn is DataFrameColumn
    /* This is the new system, which is rows. */
    Array<Boolean> rows

    action Add(text value)
        if value = undefined or value:IsEmpty()
            rows:Add(undefined)
            parent:DataFrameColumn:undefinedSize = GetUndefinedSize() + 1
            return now
        end

        check 
            text val = value:ToLowerCase()
            if val = "true"
                rows:Add(true)
            elseif val = "t"
                rows:Add(true)
            elseif val = "y"
                rows:Add(true)
            elseif val = "yes"
                rows:Add(true)
            elseif val = "1"
                rows:Add(true)
            elseif val = "false"
                rows:Add(false)
            elseif val = "f"
                rows:Add(false)
            elseif val = "n"
                rows:Add(false)
            elseif val = "no"
                rows:Add(false)
            elseif val = "0"
                rows:Add(false)
            else //We don't recognize this value, so put in undefined
                rows:Add(undefined) //add a missing value
                parent:DataFrameColumn:undefinedSize = GetUndefinedSize() + 1
            end
        detect e
            rows:Add(undefined) //add a missing value
            parent:DataFrameColumn:undefinedSize = GetUndefinedSize() + 1
        end
    end

    action Add(boolean value)
        rows:Add(value)
    end

    action SendValueTo(integer index, DataFrameColumnCalculation calculation)
        Boolean int = rows:Get(index)
        calculation:Add(int)
    end

    action IsUndefined(integer row) returns boolean
        return rows:Get(row) = undefined
    end

    action Get(integer row) returns Boolean
        return rows:Get(row)
    end

    action GetAsText(integer index) returns text
        Boolean value = rows:Get(index)
        if value = undefined
            return undefined
        else
            return "" + value:GetValue()
        end
    end

    action SetAsBoolean(integer index, boolean value)
        rows:Set(index, value)
    end

    action IsBooleanColumn returns boolean
        return false
    end

    action SetSize(integer size)
        rows:SetSize(size)
    end

    action GetSize returns integer
        return rows:GetSize()
    end

    action Swap(integer left, integer right)
        Boolean temp = undefined
        temp = rows:Get(left)
        rows:Set(left, rows:Get(right))
        rows:Set(right, temp)
    end

    action Move(integer left, integer right)
        rows:Set(right, rows:Get(left))
    end

    action Copy(integer rowStart, integer rowEnd) returns DataFrameColumn
        BooleanColumn column
        column:SetHeader(GetHeader())

        i = rowStart
        repeat while i < rowEnd
            Boolean value = rows:Get(i)
            if value = undefined
                column:rows:Add(undefined)
            else
                Boolean value2
                value2:SetValue(value:GetValue())
                column:rows:Add(value2)
            end
            i = i + 1
        end

        return column
    end

    action Copy returns DataFrameColumn
        return Copy(0, rows:GetSize())
    end

    action ToText returns text
        text result = ""
        text lf = result:GetLineFeed()
        i = 0
        repeat while i < GetSize()
            Boolean value = rows:Get(i)
            if value not= undefined
                boolean b = value:GetValue()
                result = result + b + lf
            else
                result = result + GetUndefinedText() + lf
            end
            i = i + 1
        end
        return result
    end
end