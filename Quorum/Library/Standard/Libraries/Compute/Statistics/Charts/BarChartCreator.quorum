package Libraries.Compute.Statistics.Charts

use Libraries.Compute.Statistics.DataFrameChartCreator
use Libraries.Interface.Controls.Charts.Chart
use Libraries.Compute.Statistics.DataFrame
use Libraries.Interface.Controls.Charts.BarChart
use Libraries.Compute.Statistics.DataFrameColumn
use Libraries.Containers.Array
use Libraries.Containers.HashTable

/*
    This class is used to create a Bar Chart from columns of data in a DataFrame.
    This creator is also capable of grouping data points together into one bar by
    averaging their values. Setting the name column and value column will 
    generate a Bar Chart that can be rendered in the Game Engine. 

    Attribute: Author Gabriel Contreras
    Attribute: Example

    use Libraries.Compute.Statistics.DataFrame
    use Libraries.Compute.Statistics.Charts.BarChartCreator

    DataFrame frame
    frame:Load("Data/Data.csv")

    //Create a Bar Chart and set some properties
    BarChartCreator creator
    creator:SetXColumn("Group")
    creator:SetYColumn("DT")
    creator:SetYSteps(4)
    creator:GroupByName(true)

    Chart chart = frame:CreateChart(creator)
*/
class BarChartCreator is DataFrameChartCreator
    private number maxScale = maxScale:GetMinimumValue()
    private integer xSteps = 5
    private integer ySteps = 5
    private boolean groupByName = false
    
    private integer nameColumn = -1
    private integer valueColumn = -1

    action Create(DataFrame frame) returns Chart
        BarChart chart

        //error checking
        DataFrameColumn names = frame:GetColumn(nameColumn)
        DataFrameColumn values = frame:GetColumn(valueColumn)

        if names = undefined
            alert("Could not find a column named " + nameColumn)
        end
        if values = undefined
            alert("Could not find a column named " + valueColumn)
        end

        integer size = names:GetSize()
        if size not= values:GetSize()
            alert("Column size mismatch")
        end

        //get usable arrays
        Array<text> namesArray
        Array<number> valuesArray

        ColumnToTextArray getNames
        names:Calculate(getNames)
        namesArray = getNames:GetItems()
        ColumnToNumberArray getValues
        values:Calculate(getValues)
        valuesArray = getValues:GetItems()


        //group items that have the same name 
        if groupByName
            Array<text> newNames
            Array<number> newValues
            integer groupCount = 0
            HashTable<text, number> nameHash
            i = 0
            repeat while i < size
                text name = namesArray:Get(i)
                number val = valuesArray:Get(i)
                if nameHash:HasKey(name)
                    nameHash:Set(name, nameHash:GetValue(name)+ 1)
                    integer location = newNames:GetFirstLocation(name)
                    newValues:Set(location, newValues:Get(location) + val)
                else
                    nameHash:Add(name, 1)
                    newNames:AddToEnd(name)
                    newValues:AddToEnd(val)
                    groupCount = groupCount + 1
                end
            i = i + 1
            end
            i = 0
            repeat while i < groupCount
                newValues:Set(i, newValues:Get(i) / nameHash:GetValue(newNames:Get(i)) )
            i = i + 1
            end
            namesArray = newNames
            valuesArray = newValues
            size = groupCount
        end

        //adjust values to percentage
        number max = getValues:GetItems():Get(0)
        if maxScale = max:GetMinimumValue()
            integer i = 1
            repeat while i < size
                if valuesArray:Get(i) > max
                    max = valuesArray:Get(i)
                end
            i = i + 1
            end
        else
            max = maxScale
        end
        i = 0
        repeat while i < size
            valuesArray:Set(i, valuesArray:Get(i)/max)
        i = i + 1
        end

        integer stepCounter = 0
        
        //make chart
        i = 0
        repeat while i < namesArray:GetSize()
            if xSteps = -1
                chart:AddBar(namesArray:Get(i), valuesArray:Get(i))
            else
                integer stepDistance = namesArray:GetSize() / xSteps
                if stepCounter = 0
                    chart:AddBar(namesArray:Get(i), valuesArray:Get(i))
                else
                    chart:AddBar("", valuesArray:Get(i))
                    
                end
                stepCounter = stepCounter + 1
                if stepCounter > stepDistance
                    stepCounter = 0
                end
            end
            i = i + 1
        end
        if maxScale not= maxScale:GetMinimumValue()
            chart:SetScaleMax(maxScale)
        else
            chart:SetScaleMax(max)
        end
        if ySteps not= -1
            chart:SetNumberOfSteps(ySteps)
        end
        return chart
    end
    /*
        Returns the name of the name column specified.
    */
    action GetXColumn returns integer
        return nameColumn
    end

    /*
        Returns the name of the value column specified.
    */
    action GetYColumn returns integer
        return valueColumn
    end

    /*
        Sets the name of the name column specified.
    */
    action SetXColumn(integer column)
        me:nameColumn = column
    end

    /*
        Sets the name of the name column specified.
    */
    action SetYColumn(integer column)
        me:valueColumn = column
    end

    /*
        If true the creator will combine rows of the data together if they share
        the same name and the resulting value will the mean of all entries with 
        that same name.
    */
    action GroupByName(boolean group)
        me:groupByName = group
    end
    
    action GetYSteps returns integer
        return ySteps
    end

    action GetXSteps returns integer
        return xSteps
    end

    /*
        Sets the number of steps in the scale. 
    */
    action SetYSteps(integer steps)
        ySteps = steps
    end
    /*
        Sets the number of steps in the scale. 
    */
    action SetXSteps(integer steps)
        xSteps = steps
    end

    
    /*
        Sets the maxiumum value that will appear at the top of the scale. Note 
        that the other values of the scale will be affected by the maximum.
    */
    action SetScaleMax(number max)
        maxScale = max
    end
end