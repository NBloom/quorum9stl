package Libraries.Game

// Terrain Class

// Libraries needed by Terrain
// -- also "Perlin Noise" Library



use Libraries.Compute.BitwiseOperations
use Libraries.Compute.Random
use Libraries.Compute.Vector3
use Libraries.Containers.Array
use Libraries.Containers.Table
use Libraries.Game.Graphics.VertexAttributes
use Libraries.Game.Graphics.Color
use Libraries.Game.Graphics.ColorAttribute
use Libraries.Game.Graphics.Material
use Libraries.Game.Graphics.MeshBuilder
use Libraries.Game.Graphics.Model
use Libraries.Game.Graphics.ModelBlueprint
use Libraries.Game.Graphics.ModelBuilder
use Libraries.Compute.Math
use Libraries.Compute.Frustum
use Libraries.System.File
use Libraries.System.Blueprints.FileWriterBlueprint
use Libraries.System.Blueprints.FileReaderBlueprint
use Libraries.Language.Types.Number
use Libraries.Language.Types.Text
use Libraries.Data.Formats.JavaScriptObjectNotation
use Libraries.Compute.PerlinNoise
use Libraries.Game.Collision.Shapes.Triangle3D
use Libraries.Game.Layer3D  //for physics
use Libraries.Containers.Octree.all

class Terrain is Model
    // Holds array of triangle models
    // This can easily be replaced by a table container if needed
    Array<Model> mesh

    // Holds Height values of vector points
    Table<Vector3> vectors


    // custom triangleShape
    Triangle3D triangleShape

    // Primitive Meshtypes
    constant integer POINTS = 0
    constant integer LINES = 1
    constant integer TRIANGLES = 4

    // Primitive Noisetypes
    constant integer NONOISE = 0
    constant integer PERLIN = 1

    // Default variables that can be changed as needed
    number baseHeight = 1
    integer meshType = TRIANGLES
    integer resolution = 1
    Color color
    color = color:Green()
    Octree octree

    /*
    Dictates the number of triangles to generate
    will be updated when BuildTerrain() is called
    */
    private integer rows = 1
    private integer columns = 1

    /*
    Set of private variables for EnablePhysics() 
    */
    number tWidth = 0
    number tDepth = 0

    /*
    Perlin Noise Variables
    NOTE: Our Perlin function does not like negative value parameters
    */
    number maxPerlin = 1000000      // Set Max for Perlin

    Random randNum
    number randSd = randNum:RandomNumber() * maxPerlin      // Start of with a random seed

    number noiseSeedX = randSd      // Perlin Noise seed for X-axis
    number noiseSeedZ = randSd      // Perlin Noise seed for Z-axis
    integer octaves = 2             // Used for OctavePerlin for more variety
    number persistence = 3          // Used for OctavePerlin for more variety
    number amplitude = 2            // Range of values ouput from Perlin
    number harshness = 0.2          // Dictates the harshness of the perlin noise
    Frustum frustum = undefined



    // Simple function to map a value from one range to another
    private action MapRange(number value, number inMin, number inMax, number outMin, number outMax) returns number
        number slope = 1 * (outMax - outMin) / (inMax - inMin)
        return outMin + (slope * (value - inMin))
    end

    /*
    Generates a noise seed given a number

    Atribute: Example
    use.Libraries.Game.Terrain.Terrain

    class Main is Game
        Terrain newTerrain
        newTerrain:noiseSeed(1234)
    end
    */
    action Seed(number seed)

        // I have not found a max value to give Perlin.
        // Leaving this here for ease of setting a cap later.
        if( seed > maxPerlin )
            seed = maxPerlin
        end
        noiseSeedX = seed
        noiseSeedZ = seed

    end

    /*
    Generates randomized noise seed

    Example:
    use.Libraries.Game.Terrain.Terrain

    class Main is Game
        Terrain newTerrain
        newTerrain:RandomSeed()
    end
    */
    action RandomSeed()

        Random random
        number randomSeed = random:RandomNumber() * maxPerlin

        // things get weird if x and y have different values, but it will work.
        noiseSeedX = randomSeed
        noiseSeedZ = randomSeed

    end

    /*
    Populates vector Table (of size width x depth) with a number of vectors (points in 3D space). 
    Uses the given noise type to generate the random vectors and uses the current 3D layer to 
    manipulate the 3D space. A noise type of 0 will generate a flat terrain, a noise type of 1 will 
    generate a terrain based off of perlin noise (random). The generated vectors will then form a
    3D layer of terrain based on the vector table values. 
    
    Attribute: Example

    Populates vector Table with a number of vectors (aka points) based on the x, and z value.
    If a noise type is given, then use that type of noise to generate the terrain values.
    Call

    class Main is Game
        Terrain newTerrain

        // Primitive Noisetypes
        constant integer NONOISE = 0
        constant integer PERLIN = 1

        number CanvasWidth = 15
        number CanvasDepth = 15
        integer terrainNoiseType = PERLIN

        newTerrain:BuildTerrain(CanvasWidth, CanvasDepth, terrainNoiseType, GetCurrentLayer3D())
    end
    */
    action BuildTerrain(number width, number depth, integer noiseType, Layer3D currentLayer3D)
        // ============== Create "Terrain" as Array of Models =================
        // The Width and Depth passed in are values to set the X and Z of the terrain canvas

        tWidth = width
        tDepth = depth

        columns = cast(integer, width * resolution)
        rows = cast(integer, depth * resolution)

        number sizeX = width/columns    // X-Axis size of each triangle
        number sizeZ = depth/rows       // Z-Axis size of each triangle
        number noise = 0                // Holds generated Perlin Noise values
        vectors:Empty()                 // Clears vector table
        vectors:SetSize(rows, columns)  // Sets size of vector table



        // Populate Vector Table with vector positions with heights generated by Perlin Noise
        PerlinNoise perl

        integer i = 0
        integer j = 0
        number xOff = noiseSeedX
        number zOff = noiseSeedZ
        repeat while i < rows + 1
            xOff = noiseSeedX
            j = 0
            repeat while j < columns + 1

                // Use PerlinNoise to generate values
                if noiseType = PERLIN

                    // USE ONE OF THESE
                    // WARNING: perlin noise does not like negative values for x, y, z, parameters

                    noise = perl:OctavePerlin(xOff * harshness, zOff * harshness, baseHeight, octaves, persistence)

                    noise = MapRange(noise, 0, 1, -amplitude, amplitude)    // Maps perlin output to range of amplitude

                // Choose to not generate noise, resulting in flat surface
                elseif noiseType = NONOISE
                    noise = 0
                end

                // Puts Vector into table
                Vector3 tempVector
                tempVector:Set((j * sizeX) - (width/2), (baseHeight + noise), (i * sizeZ) - (depth/2))
                vectors:Add(i, j, tempVector)

                xOff = xOff + sizeX
                j = j + 1
            end
            zOff = zOff + sizeZ
            i = i + 1
        end
       
        GenerateTerrain(currentLayer3D) //Call function to generate the terrain in 3D space

    end

    /*
    Main function that will allow us to create a 3D model of the terrain generated by the vectors
    Example:
        class Main is Game
            use Libraries.Game.Layer3D

            Terrain newTerrain
            newTerrain:GenerateTerrain(GetCurrentLayer3D())
            //GetCurrentLayer3D from the Layer3D class
    end
    */
    action GenerateTerrain(Layer3D currentLayer3D)
        //Set the objects meshType and color for use with loading

        // Empty mesh array to prep for refill
        mesh:Empty()

        // Create material
        Material material
        ColorAttribute colorAttribute
        colorAttribute:SetAttribute(colorAttribute:GetDiffuseValue(), color)
        material:Add(colorAttribute)

        // Create attribute mask
        VertexAttributes attributes
        BitwiseOperations bits
        integer mask = bits:Or(attributes:POSITION, attributes:NORMAL)  // Allows lighting to work properly

        integer i = 0
        integer j = 0
        repeat while i < rows
            j = 0
            repeat while j < columns

/*
                Add 2 Triangles to create one square

                For Reference:
                +y^
                 | /+z
                 |/
                 0---->+x

                Here is a visualization:
                    v1 .______. v3
                       |     /|
                       |    / |
                       |   /  |
                       |  /   |
                       | /    |
                       |/_____|
                    v2 `      ` v4
*/

                //Create Triangles in MeshBuilder using vector3 points for the vertices
                Vector3 v1
                Vector3 v2
                Vector3 v3
                Vector3 v4

                v1 = vectors:Get(i, j)
                v2 = vectors:Get(i+1, j)
                v3 = vectors:Get(i, j+1)
                v4 = vectors:Get(i+1, j+1)


                // Temporary Models to be placed into model array
                Model upperTriangle
                Model lowerTriangle

                // Set models to custom Triangle3D
                //upperTriangle:LoadTriangle(v1, v3, v2)
                //lowerTriangle:LoadTriangle(v3, v2, v4)
                upperTriangle:LoadTriangle(v1, v2, v3)
                lowerTriangle:LoadTriangle(v4, v3, v2)

                // Calculate the midpoints for each of the triangles
                number tempMidX = (v1:GetX() + v2:GetX() + v3:GetX())/3
                number tempUpperMidY = (v1:GetY() + v2:GetY() + v3:GetY())/3
                number tempLowerMidY = (v4:GetY() + v2:GetY() + v3:GetY())/3
                Vector3 tempYVector = GetYValues(v1:GetY(),v2:GetY(),v3:GetY(),v4:GetY())
                number tempLowestY = tempYVector:GetX()
                number tempGreatestY = tempYVector:GetY()
                number tempMiddleY = tempYVector:GetZ()
                number tempMidZ = (v1:GetZ() + v2:GetZ() + v3:GetZ())/3
                //output v1:GetZ()

                Vector3 tempV1
                Vector3 tempV4

                //Y needs to be set to 0 because the relative height is set in the meshbuilder
                tempV1:Set(tempMidX, 0, tempMidZ)
                tempV4:Set(tempMidX, 0, tempMidZ)

                //Z needs to get negated, since negative Z is inside the computer and positive Z is towards the user
                tempV1:SetZ(v1:GetZ() * -1)
                tempV4:SetZ(v4:GetZ() * -1 + 1)

                upperTriangle:SetPosition(tempV1)
                lowerTriangle:SetPosition(tempV4)
                upperTriangle:SetHeight(tempGreatestY)
                lowerTriangle:SetHeight(tempLowestY)
                upperTriangle:SetName("upperTriangle")
                lowerTriangle:SetName("lowerTriangle")

                Vector3 newV1
                newV1:SetY(v1:GetY())
                newV1:SetX(0)
                newV1:SetZ(0)
                Vector3 newV2
                newV2:SetY(v2:GetY())
                newV2:SetX(0)
                newV2:SetZ(1)
                Vector3 newV3
                newV3:SetY(v3:GetY())
                newV3:SetX(1)
                newV3:SetZ(0)
                Vector3 newV4
                newV4:SetY(v4:GetY())
                newV4:SetX(1)
                newV4:SetZ(1)

                ModelBlueprint upperBlueprint
                ModelBlueprint lowerBlueprint

                // Create ModelBuilder
                ModelBuilder newModelBuilder
                MeshBuilder newMeshBuilder

                // Build upper Triangle
                newModelBuilder:Begin()
                newMeshBuilder = newModelBuilder:AddPart("terrainTriangle", meshType, mask, material)
                //newMeshBuilder:AddTriangle(v1, v2, v3)      // Upper left portion of each square
                newMeshBuilder:AddTriangle(newV1, newV2, newV3)      // Upper left portion of each square
                upperBlueprint = newModelBuilder:End()

                // Build lower Triangle
                newModelBuilder:Begin()
                newMeshBuilder = newModelBuilder:AddPart("terrainTriangle", meshType, mask, material)
                //newMeshBuilder:AddTriangle(v4, v3, v2)      // Lower right portion of each square
                newMeshBuilder:AddTriangle(newV4,newV3,newV2)      // Lower right portion of each square
                lowerBlueprint = newModelBuilder:End()

                // Load blueprints into temporary Models
                upperTriangle:Load(upperBlueprint)
                lowerTriangle:Load(lowerBlueprint)

                //Changed upper and lower triangles to "TriangleModel" class to store coordinates
                //Added for Rendering purposes

                //upperTriangle:SetPos(v1, v2, v3)
                //lowerTriangle:SetPos(v4, v3, v2)

                // set height of triangles
                upperTriangle:SetHeight(tempGreatestY)
                lowerTriangle:SetHeight(tempGreatestY)
               

                currentLayer3D:AddToFront(upperTriangle)
                currentLayer3D:AddToFront(lowerTriangle)
                // Add models to Terrain Array
                mesh:Add(upperTriangle)
                mesh:Add(lowerTriangle)

                j = j + 1
            end
            i = i + 1
        end
    end

    /*
    Returns a vector containing min, middle, and max Y values
    
    Attribute: Example


        Vector3 v1
        Vector3 v2
        Vector3 v3
        Vector3 v4
        ...

        Vector3 yVector = GetYValues(v1:GetY(), v2:GetY(), v3:GetY(), v4:GetY())
    */
    action GetYValues(number v1, number v2, number v3, number v4) returns Vector3
        number middleY = 0
        number maxY = v1
        number minY = v1
        Vector3 returnVector

        if maxY < v2
            maxY = v2
        end
        if maxY < v3
            maxY = v3
        end
        if maxY < v4
            maxY = v4
        end

        if minY > v2
            minY = v2
        end
        if minY > v3
            minY = v3
        end
        if minY > v4
            minY = v4
        end

        middleY = minY + ((maxY-minY)/2)

        returnVector:SetX(minY)
        returnVector:SetY(maxY)
        returnVector:SetZ(middleY)
        return returnVector
    end

    // Returns the array of models - currently used in main to add all the models for displaying purposes
    action GetMesh() returns Array<Model>
        return mesh
    end

    action GetColumns() returns integer
        return columns
    end

    action GetRows() returns integer
        return rows
    end


     /*
    The EnablePhysics function will fill the octree with values based on the width (xVal)
    and depth (zVal) of the terrain.

    Atribute: Example

         class Main is Game

            use Libraries.Game.Terrain
            use Libraries.Game.Layer3D

 

            Terrain newTerrain

            newTerrain:BuildTerrain(xVal, zVal, noiseType, GetCurrentLayer3D())
            newTerrain:enableGroundPhysics(true)
            newTerrain:EnableGroundPhysics(true)
     */

      action EnablePhysics(boolean bool)

         if bool = true

             Math math

             Vector3 octpos

             octpos:Set(0,0,0)

             octree:CreateOctree(octpos,math:MaximumOf(tWidth,tDepth),4)

             octree:AddTriangles(mesh)

             octree:PruneTree()

         end

     end

 

     // Returns Octree

     action GetOctree() returns Octree

         return octree

     end

 

     action GetRenderables(Libraries.Containers.Array<Libraries.Game.Graphics.Renderable> renderables)

         octree:GetRenderables(renderables)

     end

    // Function that allows terrain code to be saved to a file using GUI implementation of File->Save Terrain.
    action SaveTerrain() returns JavaScriptObjectNotation
        JavaScriptObjectNotation json

        // Iterrate through the tables

        json:Add("Number of Rows", rows)
        json:Add("Number of Columns", columns)

        integer i = 0
        integer j = 0
        integer vectorNumber = 0

        repeat while i < rows + 1
            j = 0
            repeat while j < columns + 1
                //Puts the Row# and Column# for the vector
                JavaScriptObjectNotation tableArray
                tableArray:SetKey("Row# "+ i + " Column# " + j)
                //tableArray:SetArray()

                JavaScriptObjectNotation vectorArray

                vectorArray:SetKey("Vector# " + vectorNumber)
                //vectorArray:SetArray()
                //get the vector
                Vector3 tempVector = vectors:Get(i,j)
                //get and put the vector coordinates
                vectorArray:Add("X Coordinate", tempVector:GetX())
                vectorArray:Add("Y Coordinate", tempVector:GetY())
                vectorArray:Add("Z Coordinate", tempVector:GetZ())

                tableArray:Add(vectorArray)
                //puts into json
                json:Add(tableArray)

                vectorNumber = vectorNumber + 1

                j = j + 1
            end
            i = i + 1
        end

        return json
    end

    // Function that allows terrain code to be saved to a file using GUI implementation of File->Load Terrain.
    action LoadTerrain(JavaScriptObjectNotation json)

        rows = json:GetInteger("Number of Rows")
        columns = json:GetInteger("Number of Columns")

        // Iterrate through the tables
        integer i = 0
        integer j = 0
        integer vectorNumber = 0

        //Sets tables to be used
        Table<Vector3> tempTable
        tempTable:Empty()
        tempTable:SetSize(rows+1, columns+1)
        vectors:Empty()
        vectors:SetSize(rows+1,columns+1)

        repeat while i < rows + 1
            j = 0
            repeat while j < columns + 1

                Vector3 tempVector

                JavaScriptObjectNotation tableJson
                JavaScriptObjectNotation vectorJson
                //Gets the Row#,Column#, and Vector#
                tableJson = json:GetObject("Row# "+ i + " Column# " + j)
                vectorJson = tableJson:GetObject("Vector# " + vectorNumber)

                //Gets coordinates
                number x = vectorJson:GetNumber("X Coordinate")
                number y = vectorJson:GetNumber("Y Coordinate")
                number z = vectorJson:GetNumber("Z Coordinate")

                tempVector:Set(x,y,z)

                vectors:Set(i,j,tempVector)

                vectorNumber = vectorNumber + 1
                j = j + 1
            end
            i = i + 1
        end
    end

end
