package Libraries.Interface.Controls

use Libraries.Game.Graphics.Font
use Libraries.Game.Graphics.Label
use Libraries.Game.Graphics.Color
use Libraries.Game.Graphics.Gradient
use Libraries.Interface.AccessibilityManager
use Libraries.Interface.Behaviors.Behavior
use Libraries.Interface.Events.BehaviorEvent
use Libraries.Interface.Layouts.FlowLayout
use Libraries.Interface.Layouts.LayoutProperties
use Libraries.Interface.Views.View2D
use Libraries.Interface.Views.ToggleView
use Libraries.Interface.Views.CheckboxView

class Checkbox is ToggleButton

    Icon buttonIcon
    Label label
    Color checkColor

    on create
        // The layout should already be present, courtesy of our inheritance of Button.
        FlowLayout layout = cast(FlowLayout, GetLayout())
        layout:SetIgnoreHidden(false)
        layout:SetSingleLine(true)

        LayoutProperties properties = GetDefaultLayoutProperties()
        properties:SetHorizontalLayoutMode(properties:FIT_CONTENTS)
        properties:SetVerticalLayoutMode(properties:FIT_FONT)
        
        Color color
        Color white = color:White()
        Color gray = color:CustomColor(211.0/255.0, 211.0/255.0, 211.0/255.0, 1)
        Gradient background
        background:Set(white, white, gray, gray)
        Color selection = color:CustomColor(59.0/255.0, 153/255.0, 252/255.0, 1) 
        Color border = color:CustomColor(178.0/255.0, 178.0/255.0, 178.0/255.0, 1)
        Color selectionBorder = color:CustomColor(151.0/255.0, 197/255.0, 245/255.0, 1)

        properties:SetBackgroundColor(background)
        properties:SetSelectionColor(selection)
        properties:SetBorderColor(border)
        properties:SetSelectionBorderColor(selectionBorder)
        properties:SetBorderThickness(2)

        // The properties already contain a loaded Font from the Button constructor.
        label:SetFont(properties:GetFont())
        label:SetFontSize(properties:GetFontSize())

        LayoutProperties iconProperties = buttonIcon:GetDefaultLayoutProperties()
        iconProperties:SetHorizontalLayoutMode(iconProperties:MAINTAIN_ASPECT_RATIO)
        iconProperties:SetVerticalLayoutMode(iconProperties:STANDARD)
        iconProperties:SetPercentageWidth(1.0)
        iconProperties:SetPercentageHeight(1.0)

        label:SetLeftPadding(6)

        Add(buttonIcon)
        Add(label)

        checkColor = color:White()

        SetAccessibilityCode(parent:Item:CHECKBOX)
    end

    action SetToggleState(boolean toggleState)
        /*
        This overwrites the ToggleButton implementation of SetToggleState.
        It is nearly identical to the ToggleButton implementation, except that
        it also tries to toggle the view of the Button icon.
        */
        if parent:ToggleButton:toggled = toggleState
            return now
        end

        ButtonGroup group = GetButtonGroup()
        if toggleState = true and group not= undefined
            group:DeselectAll()
        end

        parent:ToggleButton:toggled = toggleState
        if buttonIcon:GetView2D() is ToggleView
            ToggleView content = cast(ToggleView, buttonIcon:GetView2D())
            content:SetToggleState(toggleState)
        end

        Behavior behavior = GetActivationBehavior()
        if toggleState = true
            OnToggleOn()
            if behavior not= undefined
                BehaviorEvent event
                event:SetItem(me)
                behavior:Run(event)
            end
        else
            OnToggleOff()
            if behavior not= undefined
                behavior:Dispose()
            end
        end

        AccessibilityManager accessibility = parent:ToggleButton:manager:GetAccessibilityManager()
        if accessibility not= undefined
            accessibility:ToggleButtonToggled(me)
        end
    end

    /*
    This action is used to load the graphical components of the Control. This is
    handled automatically by the Game engine as needed, and most users shouldn't
    need to use this action directly.
    */
    action LoadGraphics(LayoutProperties properties)
        parent:Control:LoadGraphics(properties)

        View2D buttonView = buttonIcon:GetView2D()
        View2D propertiesView = properties:GetView2D()

        if propertiesView not= undefined and buttonView not= propertiesView
            buttonIcon:SetView2D(propertiesView)
            buttonView = propertiesView
        end

        if label:GetSize() not= properties:GetFontSize()
            label:SetSize(properties:GetFontSize())
        end

        if label:GetText() not= properties:GetLabelText()
            label:SetText(properties:GetLabelText())
        end

        if buttonView = undefined
            CheckboxView checkboxView
            checkboxView:SetCheckColor(checkColor)
            checkboxView:Initialize(label:GetLineHeight() / 2, properties)
            buttonIcon:SetView2D(checkboxView)
            checkboxView:SetToggleState(parent:ToggleButton:toggled)
        end
    end

    /*
    This action sets the color of the checkmark in the Checkbox.
    */
    action SetCheckColor(Color color)
        checkColor = color

        View2D buttonView = buttonIcon:GetView2D()
        if buttonView is CheckboxView
            CheckboxView checkView = cast(CheckboxView, buttonView)
            checkView:SetCheckColor(checkColor)
        end
    end
end