package Libraries.Interface.Controls

use Libraries.Containers.Array
use Libraries.Containers.Iterator

class MenuPanelBlueprint is Control
    
    Array<MenuItem> menuItems

    number iconRegionWidth = 0
    number labelRegionWidth = 0
    number shortcutRegionWidth = 0
    number arrowRegionWidth = 0

    on create
        SetName("Menu Panel")
        SetZ(-1)
    end

    action SetIconRegionWidth(number width)
        iconRegionWidth = width
    end

    action GetIconRegionWidth returns number
        return iconRegionWidth
    end

    action SetLabelRegionWidth(number width)
        labelRegionWidth = width
    end

    action GetLabelRegionWidth returns number
        return labelRegionWidth
    end

    action SetShortcutRegionWidth(number width)
        shortcutRegionWidth = width
    end

    action GetShortcutRegionWidth returns number
        return shortcutRegionWidth
    end

    action SetArrowRegionWidth(number width)
        arrowRegionWidth = width
    end

    action GetArrowRegionWidth returns number
        return arrowRegionWidth
    end

    action Add(MenuItem item)
        menuItems:Add(item)
        parent:Control:Add(item)
    end

    action Add(integer index, MenuItem item)
        menuItems:Add(index, item)
        parent:Control:Add(index, item)
    end

    action Get(integer index) returns MenuItem
        return menuItems:Get(index)
    end

    action GetFromEnd returns MenuItem
        return menuItems:GetFromEnd()
    end

    action Remove(MenuItem item)
        menuItems:Remove(item)
        parent:Control:Remove(item)
    end

    action Remove(integer index) returns Item2D
        menuItems:RemoveAt(index)
        return parent:Control:Remove(index)
    end

    action GetSize returns integer
        return menuItems:GetSize()
    end

    action IsEmpty returns boolean
        return menuItems:IsEmpty()
    end

    action GetIterator returns Iterator<MenuItem>
        return menuItems:GetIterator()
    end

    blueprint action IsScrollable returns boolean

end